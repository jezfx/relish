<?php define('DOCUMENT_ROOT', dirname(realpath(__FILE__)).'/'); ?>
<?php include(DOCUMENT_ROOT . 'includes/global.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include(DOCUMENT_ROOT . 'includes/header.php');?>
</head>
<body>
	<div class="wrapper">
		<section class="sidebar">
			<div class="branding">
				<div class="logo">

					<figure>
						<img id="pngLogo" src="<?php echo $siteurl;?>images/logo.png" alt="Relish Cafe Bar, Hartford">
					</figure>		

					<div id="logo">
						<?php include(DOCUMENT_ROOT . 'images/logo.svg'); ?>
					</div>
				</div>
			</div>
			<div class="navigation">
				<nav>
					<?php include(DOCUMENT_ROOT . 'includes/menu.php'); ?>
				</nav>
			</div>
			<div class="contact-details">
				<p class="icon icon-phone"><span><?php echo $telephone;?></span></p>
				<p>
					35-37 School Lane <br/>
					Hartford <br/>
					Cheshire <br/>
					CW8 1NP
				</p>					
			</div>
			<div class="social">
				<?php include('includes/social.php'); ?>
			</div>
		</section>

		<section class="content">

			<section class="slider-promo">
				<ul id="slider">
					<li>
						<div class="caption-container">
							<div class="cf">
								<h1>Saturday 28th April</h1>
							</div>
							<h2>live music from andy dunn &amp; friends</h2>							
						</div>

						<img src="<?php echo $siteurl;?>images/banners/banner1.jpg" alt="">
					</li>
					<li>
						<div class="caption-container">
							<h1>Great food, great times, GREAT!</h1>
							<h2>At expedita iure quas porro maiores facilis!</h2>							
						</div>
					
						<img src="<?php echo $siteurl;?>images/banners/banner2.jpg" alt="">
					</li>
				</ul>
			</section>

			<div class="promo-callout">
				<h1>Welcome to Hartford</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat minus inventore, qui ipsam tenetur error maiores, exercitationem perspiciatis asperiores consectetur optio soluta cum quas nesciunt alias totam perferendis itaque commodi.</p>
			</div>

			<div class="inner">
			
				<div class="box">
					<div class="image">
						<figure>
							<img src="http://placehold.it/360x240/333333/ffffff/" alt="">
						</figure>
					</div>					
					<div class="content">
						
					</div>
				</div>
				
				<div class="box">
					<div class="image">
						<figure>
							<img src="http://placehold.it/360x240/333333/ffffff/" alt="">
						</figure>
					</div>					
					<div class="content">
						
					</div>
				</div>
				<div class="box">
					<div class="image">
						<figure>
							<img src="http://placehold.it/360x240/333333/ffffff/" alt="">
						</figure>
					</div>					
					<div class="content">
						
					</div>
				</div>
				
				<div class="box">
					<div class="image">
						<figure>
							<img src="http://placehold.it/360x240/333333/ffffff/" alt="">
						</figure>
					</div>					
					<div class="content">
						
					</div>
				</div>

			</div>
		</section>	
	</div>


	<script src="<?php echo $siteurl;?>js/script.production.min.js"></script>
</body>
</html>