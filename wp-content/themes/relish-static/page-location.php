<?php define('DOCUMENT_ROOT', dirname(realpath(__FILE__)).'/'); ?>
<?php include(DOCUMENT_ROOT . '../includes/global.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include(DOCUMENT_ROOT . '../includes/header.php');?>
</head>
<body>
	<div class="wrapper">
		<section class="sidebar">
			<div class="branding">
				<div class="logo">

					<figure>
						<img id="pngLogo" src="<?php echo $siteurl;?>images/logo.png" alt="Relish Cafe Bar, Hartford">
					</figure>	

					<div id="logo">
						<?php include(DOCUMENT_ROOT . '../images/logo.svg'); ?>
					</div>
				</div>
			</div>
			<div class="navigation">
				<nav>
					<?php include(DOCUMENT_ROOT . '../includes/menu.php'); ?>
				</nav>
			</div>
			<div class="contact-details">
				<p class="icon icon-phone"><span><?php echo $telephone;?></span></p>
				<p>
					35-37 School Lane <br/>
					Hartford <br/>
					Cheshire <br/>
					CW8 1NP
				</p>					
			</div>
			<div class="social">
				<?php include(DOCUMENT_ROOT . '../includes/social.php'); ?>
			</div>
		</section>

		<section class="content">
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDIWczxoHkcxeLWqLI92edRp-AUsis8l6Q"></script>
			<script type="text/javascript">

			function createMap() {
				var relishAddress = "Relish Hartford <br/> 35-37 School Lane <br/> Hartford <br/> CW8 1NP";
				var hartford = new google.maps.LatLng(53.2391328,-2.5853191);
				var relish = new google.maps.LatLng(53.245534,-2.542275,17);
				var marker;
				var map;
			  	var mapStyle = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#465660"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}];
			    var image = '../images/marker.png';
			    var logo = '../images/logo.png';
				var mapOptions = {
				    zoom: 16,
				    center: relish,
				    styles: mapStyle,
				    scrollwheel: false
				};

				map = new google.maps.Map(document.getElementById('map-canvas'),
				          mapOptions);
				
				var infowindow = new google.maps.InfoWindow({
				      content: "<div class='info-window'><img width='100' height='100' src='"+logo+"'/><p>"+relishAddress+"</p></div>"
				});

				marker = new google.maps.Marker({
				    map: map,
				    animation: google.maps.Animation.DROP,
				    position: relish,
				    title: "Relish Hartford",
				    icon: image
				});
				  
				google.maps.event.addListener(marker, 'click', function() {
				    infowindow.open(map, marker);
				});
			}
			// make it
			google.maps.event.addDomListener(window, 'load', createMap);

			</script>

			<section class="google-map">
				<div id="pane" class="location-pane">
					<div id="remove">
						<span class="icon-cancel"></span>
					</div>
					<h3>Come and see us</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consequatur sapiente cumque, iure odit libero voluptates, praesentium laudantium et.</p>
					<ul>
						<li>
							<p class="icon-phone"><span><strong>01606 871 140</strong></span></p><br/>
						</li>
						<li>
							<p class="icon-pin"><span><strong>Relish Cafe Bar</strong></span></p>
							<p class="address-below">
								35-37 School Lane <br/>
								Hartford <br/>
								Cheshire <br/>
								CW8 1NP
							</p>	
						</li>
						<li></li>
					</ul>
				</div>
				<div id="map-canvas"/>

			</section>	
		</section>
	</div>

	<script src="<?php echo $siteurl;?>js/script.production.min.js"></script>
</body>
</html>