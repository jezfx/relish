<?php define('DOCUMENT_ROOT', dirname(realpath(__FILE__)).'/'); ?>
<?php include(DOCUMENT_ROOT . '../includes/global.php');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include(DOCUMENT_ROOT . '../includes/header.php');?>
</head>
<body>
	<div class="wrapper">
		<section class="sidebar">
			<div class="branding">
				<div class="logo">

					<figure>
						<img id="pngLogo" src="<?php echo $siteurl;?>images/logo.png" alt="Relish Cafe Bar, Hartford">
					</figure>	

					<div id="logo">
						<?php include(DOCUMENT_ROOT . '../images/logo.svg'); ?>
					</div>
				</div>
			</div>
			<div class="navigation">
				<nav>
					<?php include(DOCUMENT_ROOT . '../includes/menu.php'); ?>
				</nav>
			</div>
			<div class="contact-details">
				<p class="icon icon-phone"><span><?php echo $telephone;?></span></p>
				<p>
					35-37 School Lane <br/>
					Hartford <br/>
					Cheshire <br/>
					CW8 1NP
				</p>					
			</div>
			<div class="social">
				<?php include(DOCUMENT_ROOT . '../includes/social.php'); ?>
			</div>
		</section>

		<section class="content">
			<div class="inner">
				<h1>About</h1>	
				<h2>
					Lorem ipsum dolor sit amet
				</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio amet, doloremque repudiandae esse tempora consequatur, maxime quos vitae molestias aliquam minima quas blanditiis rerum magni a aliquid temporibus, aut fuga!
				</p>
				
				<figure>
					<img class="feature" src="<?php echo $siteurl;?>images/example-image-about.jpg" alt="">
				</figure>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, sint eligendi possimus laboriosam reprehenderit eius harum odio incidunt, sit voluptatibus recusandae atque aperiam totam architecto laborum ipsa qui nam optio.
				</p>	
				<h2>
					But there's more
				</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit blanditiis magnam accusamus ipsum laudantium nobis, beatae. Voluptates fugiat eaque minus repudiandae impedit, itaque in non, corporis cumque voluptas eveniet corrupti.</p>
				<h3>
					And even more!
				</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum eaque suscipit aliquid vitae ad, corrupti iure reprehenderit accusantium repellat saepe quisquam recusandae perspiciatis ipsam in ipsa quibusdam rem quasi! Aut.</p>			
			</div>
		</section>
	</div>

	<script src="<?php echo $siteurl;?>js/script.production.min.js"></script>
</body>
</html>