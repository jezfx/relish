<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <section class="content">
 *
 * @package relish
 */
?><!DOCTYPE html>
	<!--[if !(IE 7) & !(IE 8)]><!-->
	<html <?php language_attributes(); ?>>
	<!--<![endif]-->
	<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<?php // IE10 to behave like 9 as it wont work with the SVG and conditional statements are ignored ?>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
	
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link rel="icon" href="#" type="image/x-icon" />
	<link rel="shortcut icon" href="#" type="image/x-icon" />
	<link href="#" sizes="152x152" rel="apple-touch-icon">
	<link href="#" sizes="144x144" rel="apple-touch-icon">
	<link href="#" sizes="120x120" rel="apple-touch-icon">
	<link href="#" sizes="114x114" rel="apple-touch-icon">
	<link href="#" sizes="76x76" rel="apple-touch-icon">
	<link href="#" sizes="72x72" rel="apple-touch-icon">
	<link href="#" sizes="57x57" rel="apple-touch-icon">
	
	<?php 

		wp_head(); 

		//======================================================================
		// CUSTOM CSS STYLES
		//======================================================================

		$custom_styles;

		//-----------------------------------------------------
		// Custom Background Image
		//-----------------------------------------------------

		$custom_background_image = get_field( 'custom_background_image' );

		if (!empty( $custom_background_image )) {
			$custom_styles .= 'body {background-image: url(' . $custom_background_image . ');}';
		}

		//-----------------------------------------------------
		// Print Custom Styles
		//-----------------------------------------------------

		if (!empty( $custom_styles )) {
			echo sprintf( '<style type="text/css">%s</style>', $custom_styles );
		}
	?>

</head>

<body <?php body_class(); ?>>
	<div class="wrapper">

		<section class="sidebar">
			<div class="branding">
				<div class="logo">
					<figure>
						<img id="pngLogo" src="<?= get_template_directory_uri();?>/images/logo.png" alt="Relish Cafe Bar, Hartford">
					</figure>		

					<?php if ( is_front_page() ): ?>
						<div id="logo">
							<?php include( get_template_directory() . '/images/logo.svg' ); ?>
						</div>						
					<?php endif ?>
				</div>
			</div>
			<div class="navigation">
				<nav id="site-navigation" class="main-navigation" role="navigation">
					<ul class="mobile-quicklinks">
						<li><a href="tel:<?= get_theme_mod('phone_number'); ?>"><i class="icon-phone"></i></a></li>
						<li><a href="/location#map"><i class="icon-pin"></i></a></li>
					</ul>
					<button class="menu-toggle"><i class="burger-menu">f</i></button>
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>					
				</nav>
			</div>
			<div class="contact-details">
				<a href="tel:<?= get_theme_mod('phone_number');?>" class="phone-number">
					<?= preg_replace('/^.{5}/', "$0 ", get_theme_mod('phone_number'));?>
				</a>
				<dl class="opening-times">
					<dt>Mon - Weds</dt>
					<dd>09:00 - 21:00</dd>

					<dt>Thurs - Sat</dt>
					<dd>09:00 - 22:30</dd>

					<dt>Sun</dt>
					<dd>10:00 - 18:00</dd>										
				</dl>
			</div>
			<div class="social">
				<div class="social-links">
					<ul class="cf">
						<li><a class="icon icon-twitter" href="#"></a></li>
						<li><a class="icon icon-facebook" href="#"></a></li>
						<li><a class="icon icon-linkedin" href="#"></a></li>
					</ul>	
				</div>				
			</div>
		</section>
		
		<section class="content">
		
