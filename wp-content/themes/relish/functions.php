<?php
/**
 * relish functions and definitions
 *
 * @package relish
 */


require get_template_directory() . '/functions/_setup.php';
require get_template_directory() . '/functions/_widgets.php';
require get_template_directory() . '/functions/_loader.php';
require get_template_directory() . '/functions/_advanced-custom-fields.php';


//-----------------------------------------------------
// Custom Dashbaord Pages (not needed anymore)
//-----------------------------------------------------

// require get_template_directory() . '/functions/_custom-dashboard-pages.php';


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php'; 

?>