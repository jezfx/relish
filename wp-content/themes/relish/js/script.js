$(function() {
	$("#slider").owlCarousel({
	 	navigation : true, // Show next and prev buttons
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem: true,
		autoPlay : 5000
	});
});

$(function() {

	function animateSVG(path, animation) {

		var path = document.querySelector(path);
		var length = path.getTotalLength();
		// Clear any previous transition
		path.style.transition = path.style.WebkitTransition = 'none';
		// Set up the starting positions
		path.style.strokeDasharray = length + ' ' + length;
		path.style.strokeDashoffset = length;
		// Trigger a layout so styles are calculated & the browser
		// picks up the starting position before animating
		path.getBoundingClientRect();
		// Define our transition
		path.style.transition = path.style.WebkitTransition = animation;
		// Go!
		path.style.strokeDashoffset = '0';
		// on the animation end, fade in logo		
		$('#svgLogo').bind('transitionend webkitTransitionEnd', function(e){	    	
	    	e.stopPropagation();	    		    
			$('#logo').attr('class', 'hide');
			$('#pngLogo').addClass('show');
		});			
	}

	var isHome = $('body.home');
	var page_width = $(document).width();

	if (isHome.length > 0 && page_width > 640) {
		animateSVG('.brand-flick', 'stroke-dashoffset 2s ease-in-out');
		animateSVG('.brand-type', 'stroke-dashoffset 3s ease-in-out');
	} else {
		$('#logo').attr('class', 'hide');
		$('#pngLogo').addClass('show');
	};

});

$(function() {
	var content_pane = $('.content'); 
	if( content_pane.hasClass('pt-page-rotatePullLeft') ) {
		console.log('it has the class of pt-page-rotatePullLeft');
	} else {
		$(content_pane).addClass('pt-page-rotatePullLeft pt-page-delay300');
	}
});

$(function() {
	$('#remove').click(function(e) {
		
		$('#pane').addClass('pt-page-scaleDown');

		e.preventDefault();		
		
		$('#pane').bind('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function(e){
	    	e.stopPropagation();
	    	$(this).hide();
		});
	});
});

