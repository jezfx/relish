<?php
/**
 * relish Theme Customizer
 *
 * @package relish
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function relish_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';


  //-----------------------------------------------------
  // Site info section
  //-----------------------------------------------------


  $wp_customize->add_section( 'info_section', array(
    'title'    => __( 'Site Info' ),
    'priority' => 20,
  ));
 
  $wp_customize->add_setting( 'phone_number', array(
    'default'    => '01606 871140',
    'type'       => 'theme_mod',
    'transport'  => 'refresh'
  ));

  $wp_customize->add_control( 'phone_number', array(
    'label'      => __( 'Phone Number' ),
    'section'    => 'info_section',
    'type'       => 'text'
  ));

}
add_action( 'customize_register', 'relish_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function relish_customize_preview_js() {
	wp_enqueue_script( 'relish_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'relish_customize_preview_js' );
