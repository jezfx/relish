var gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    imageop = require('gulp-image-optimization'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    less = require('gulp-less'),
    // connect = require('gulp-connect');
    path = require('path');    
   
    // LESS
    gulp.task('less', function () {
        // Order is important. 
        return gulp.src([
            'css/vendor/normalize.less',
            'css/vendor/animate.css',
            'css/vendor/owl.carousel.css',
            'css/vendor/owl.theme.css',
            'css/vendor/owl.transitions.css',
            'css/style.less'
            ])
            .pipe(less({
                paths: [ path.join(__dirname, 'less', 'includes') ]
            }))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'Firefox <= 20'))
        .pipe(concat('production.css'))
        .pipe(gulp.dest('css/'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(minifycss())
        .pipe(gulp.dest('css/'))
        .pipe(notify({ message: '===== LESS Built OK ======' }));
    });
    // Javascript
    gulp.task('javascript', function() {
        // Order is important. 
        return gulp.src([
            'js/jquery.1.9.0.min.js',
            'js/owl.carousel.js',
            'js/social-share-buttons.js',
            'js/navigation.js',
            'js/script.js',
            'js/modernizr.custom.14388.js'
        ])
        .pipe(jshint.reporter('default'))
        .pipe(concat('script.production.js'))
        .pipe(gulp.dest('js/'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('js/'))
        .pipe(notify({ message: '====== JAVASCRIPT Built OK =====' }));
    });

    gulp.task('images', function(cb) {
        gulp.src(['src/**/*.png','src/**/*.jpg','src/**/*.gif','src/**/*.jpeg']).pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })).pipe(gulp.dest('images')).on('end', cb).on('error', cb);
    });
     

    gulp.task('watch', function() {
        // Watch .js files
        // gulp.watch('js/*.js', ['javascript']);
        gulp.watch('css/*.less', ['less']);
        gulp.watch('css/pages/*.less', ['less']);
        gulp.watch('css/elements/*.less', ['less']);
        // gulp.watch('css/*.css', ['less']);
        // gulp.start('less');
    });

    // gulp.task('connect', function() {
    //   connect.server({
    //     root: 'app',
    //     livereload: true
    //   });
    // });

    gulp.task('default', function() {
        // default gulps
        gulp.start('less', 'javascript');

    });