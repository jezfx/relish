<?php
/**
 * @package relish
 */
?>

<article <?php post_class(); ?>>
  <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>

  <?php if ( 'post' == get_post_type()) : ?>
    <p class="post-info"><?= get_the_date() . ' | ' . ucfirst(get_the_author()) . ' | ' . relish_get_categories(); ?></p>
  <?php endif; ?>

  <?php if (has_post_thumbnail()) {
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');
  } ?>
	
	<?php if (has_post_thumbnail()): ?>
    <a href="<?php the_permalink(); ?>" style="background-image:url(<?= $thumb[0] ?>)" class="thumbnail"></a>
	<?php endif ?>
  
  <?php if ( 'post' == get_post_type() ) : ?>
    <p class="exerpt"><?php echo word_count(get_the_content(), 20); ?></p>
  <?php else: ?>  
    <?php the_content(); ?>
  <?php endif; ?>
</article>