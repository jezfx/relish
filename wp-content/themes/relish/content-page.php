<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package relish
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
  <header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->  

  <?php if (!empty($page_intro_paragraph)): ?>
    <p class="page-intro-paragraph">
      <?= get_field('paragraph'); ?>
    </p>  
  <?php endif; ?>  

		<?php the_content(); ?>
</article>
