<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

get_header(); ?>

    <script type="text/javascript">

    var templateDir = "<?= get_stylesheet_directory_uri() ?>";

    function createMap() {
      var relishAddress = "Relish Hartford <br/> 35-37 School Lane <br/> Hartford <br/> CW8 1NP";
      var hartford = new google.maps.LatLng(53.2391328,-2.5853191);
      var relish = new google.maps.LatLng(53.245534,-2.542275,17);
      var marker;
      var map;
        var mapStyle = [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#465660"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}];

        var image = templateDir + '/images/marker.png';
        var logo = templateDir + '/images/logo.png';

        var isDraggable = $(document).width() > 480 ? true : false; // If document (your website) is wider than 480px, isDraggable = true, else isDraggable = false

      var mapOptions = {
          zoom: 16,
          center: relish,
          styles: mapStyle,
          scrollwheel: false,
          draggable: isDraggable,
      };



      map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
      
      var infowindow = new google.maps.InfoWindow({
            content: "<div class='info-window'><img width='100' height='100' src='"+logo+"'/><p>"+relishAddress+"</p></div>"
      });

      marker = new google.maps.Marker({
          map: map,
          animation: google.maps.Animation.DROP,
          position: relish,
          title: "Relish Hartford",
          icon: image
      });
        
      google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map, marker);
      });
    }
    // make it
    google.maps.event.addDomListener(window, 'load', createMap);

    </script>

    <section class="google-map">
      <div id="pane" class="location-pane">
        <div id="remove">
          <span class="icon-cancel"></span>
        </div>
        <h3>Come and see us</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consequatur sapiente cumque, iure odit libero voluptates, praesentium laudantium et.</p>
        <ul>
          <li>          
            <p class="icon-phone"><span><strong><a href="tel:<?= get_theme_mod('phone_number'); ?>"><?= get_theme_mod('phone_number'); ?></a></strong></span></p><br/>
          </li>
          <li>
            <p class="icon-pin"><span><strong>Relish Cafe Bar</strong></span></p>
            <p class="address-below">
              35-37 School Lane <br/>
              Hartford <br/>
              Cheshire <br/>
              CW8 1NP
            </p>  
          </li>
          <li></li>
        </ul>
      </div>
      <div id="map-canvas"/>

    </section> 

<?php get_footer(); ?>