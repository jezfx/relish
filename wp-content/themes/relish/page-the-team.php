<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

global $post; 

//-----------------------------------------------------
// Page Intro Paragraph
//-----------------------------------------------------

$page_intro_paragraph = get_field('page_intro_paragraph', $post->ID);


//-----------------------------------------------------
// Get Team Members
//-----------------------------------------------------

$args = array('post_type'=> 'team-member');
$team_members = new WP_Query($args);

get_header(); ?>

      
      <div class="inner">
        
        <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          
          <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
          </header><!-- .entry-header -->

          <?php if (!empty($page_intro_paragraph)): ?>
            <p class="page-intro-paragraph">
              <?= get_field('paragraph'); ?>
            </p>  
          <?php endif; ?>  

          <?php if ( $team_members->have_posts() ) : while ( $team_members->have_posts() ) : $team_members->the_post(); ?>

            <article <?php post_class(); ?>>

              <?php the_title( '<h3 class="entry-title"><span>', '</span></h3>' ); ?>

              <?php if (has_post_thumbnail()): ?>
                <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full'); ?>
                <div style="background-image:url(<?= $thumb[0] ?>)" class="thumbnail"></div>
              <?php endif ?>
              
              <?php the_content(); ?>  

            </article>

          <?php endwhile; ?>
          
          <!-- post navigation -->

          <?php endif; ?>
            
        </section>
         
      </div>    

<?php get_footer(); ?>
