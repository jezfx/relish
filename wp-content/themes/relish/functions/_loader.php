<?php 
  
  /**
   * Enqueue scripts and styles.
   */
  function relish_scripts() {

    global $wp_styles;

    //-----------------------------------------------------
    // CSS
    //-----------------------------------------------------    

    // main stylesheet
    wp_enqueue_style( 'relish-style', get_template_directory_uri() . '/css/production.css' );
    
    // IE fixes
    $wp_styles->add('ie-stylesheet', get_template_directory_uri() . '/css/ie.css');
    $wp_styles->add_data('ie-stylesheet', 'conditional', 'IE');
    $wp_styles->enqueue(array('ie-stylesheet'));

    // < IE8 fixes
    $wp_styles->add('ie8-stylesheet', get_template_directory_uri() . '/css/ie8.css');
    $wp_styles->add_data('ie8-stylesheet', 'conditional', 'lt IE 9');
    $wp_styles->enqueue(array('ie8-stylesheet'));    


    //-----------------------------------------------------
    // JS 
    //-----------------------------------------------------

    // main js file
    wp_enqueue_script('js-production', get_template_directory_uri() . '/js/script.production.js', '1' ,0, true );
    
    // location page scripts
    if (is_page('location')) {
      wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDIWczxoHkcxeLWqLI92edRp-AUsis8l6Q', array(), 1, false);
    }
  }
  add_action( 'wp_enqueue_scripts', 'relish_scripts' );