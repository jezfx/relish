<?php 

//-----------------------------------------------------
// Homepage Banners
//-----------------------------------------------------

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_homepage-banner-slides',
    'title' => 'Homepage Banner Slides',
    'fields' => array (
      array (
        'key' => 'field_545bff6fdc872',
        'label' => 'Add to homepage banner',
        'name' => 'add_to_homepage_banner',
        'type' => 'true_false',
        'message' => 'Add this post to the homepage banner',
        'default_value' => 0,
      ),
      array (
        'key' => 'field_545bffd2dc873',
        'label' => 'Banner Main title',
        'name' => 'main_title',
        'type' => 'text',
        'conditional_logic' => array (
          'status' => 1,
          'rules' => array (
            array (
              'field' => 'field_545bff6fdc872',
              'operator' => '==',
              'value' => '1',
            ),
          ),
          'allorany' => 'all',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_545c0034dc874',
        'label' => 'Banner Sub title',
        'name' => 'sub_title',
        'type' => 'text',
        'conditional_logic' => array (
          'status' => 1,
          'rules' => array (
            array (
              'field' => 'field_545bff6fdc872',
              'operator' => '==',
              'value' => '1',
            ),
          ),
          'allorany' => 'all',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'formatting' => 'html',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_545c003edc875',
        'label' => 'Banner Image',
        'name' => 'image',
        'type' => 'image',
        'instructions' => 'Optimal image size 720 x 300',
        'conditional_logic' => array (
          'status' => 1,
          'rules' => array (
            array (
              'field' => 'field_545bff6fdc872',
              'operator' => '==',
              'value' => '1',
            ),
          ),
          'allorany' => 'all',
        ),
        'save_format' => 'object',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
      array (
        'key' => 'field_545c0fdf93609',
        'label' => 'Banner Theme',
        'name' => 'theme',
        'type' => 'select',
        'conditional_logic' => array (
          'status' => 1,
          'rules' => array (
            array (
              'field' => 'field_545bff6fdc872',
              'operator' => '==',
              'value' => '1',
            ),
          ),
          'allorany' => 'all',
        ),
        'choices' => array (
          'dark' => 'Dark',
          'light' => 'Light',
          'christmas' => 'Christmas',
        ),
        'default_value' => 'dark : Dark',
        'allow_null' => 0,
        'multiple' => 0,
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'acf_after_title',
      'layout' => 'no_box',
      'hide_on_screen' => array (
        0 => 'discussion',
        1 => 'comments',
        2 => 'format',
        3 => 'tags',
        4 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
}


//-----------------------------------------------------
// Pages
//-----------------------------------------------------

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_pages',
    'title' => 'Pages',
    'fields' => array (
      array (
        'key' => 'field_545f7dabaac79',
        'label' => 'Page Intro Paragraph?',
        'name' => 'page_intro_paragraph',
        'type' => 'true_false',
        'message' => '',
        'default_value' => 0,
      ),
      array (
        'key' => 'field_545f7d88fc085',
        'label' => 'Paragraph',
        'name' => 'paragraph',
        'type' => 'textarea',
        'conditional_logic' => array (
          'status' => 1,
          'rules' => array (
            array (
              'field' => 'field_545f7dabaac79',
              'operator' => '==',
              'value' => '1',
            ),
          ),
          'allorany' => 'all',
        ),
        'default_value' => '',
        'placeholder' => '',
        'maxlength' => '',
        'rows' => '',
        'formatting' => 'br',
      ),
      array (
        'key' => 'field_54663c8db23bd',
        'label' => 'Custom Background Image',
        'name' => 'custom_background_image',
        'type' => 'image',
        'save_format' => 'url',
        'preview_size' => 'thumbnail',
        'library' => 'all',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'acf_after_title',
      'layout' => 'no_box',
      'hide_on_screen' => array (
      ),
    ),
    'menu_order' => 0,
  ));
}

//-----------------------------------------------------
// Menus
//-----------------------------------------------------

if(function_exists("register_field_group"))
{
  register_field_group(array (
    'id' => 'acf_menu-items',
    'title' => 'Menu Items',
    'fields' => array (
      array (
        'key' => 'field_5465478f0a4e9',
        'label' => 'Menu PDF',
        'name' => 'menu_pdf',
        'type' => 'file',
        'save_format' => 'object',
        'library' => 'all',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'restaurant-menu',
          'order_no' => 0,
          'group_no' => 0,
        ),
      ),
    ),
    'options' => array (
      'position' => 'normal',
      'layout' => 'no_box',
      'hide_on_screen' => array (
        0 => 'permalink',
        1 => 'the_content',
        2 => 'excerpt',
        3 => 'custom_fields',
        4 => 'discussion',
        5 => 'comments',
        6 => 'revisions',
        7 => 'slug',
        8 => 'author',
        9 => 'format',
        10 => 'categories',
        11 => 'tags',
        12 => 'send-trackbacks',
      ),
    ),
    'menu_order' => 0,
  ));
}
