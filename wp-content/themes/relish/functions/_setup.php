<?php
  /**
   * Set the content width based on the theme's design and stylesheet.
   */
  if ( ! isset( $content_width ) ) {
    $content_width = 640; /* pixels */
  }

  if ( ! function_exists( 'relish_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function relish_setup() {

    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on relish, use a find and replace
     * to change 'relish' to the name of your theme in all the template files
     */
    load_theme_textdomain( 'relish', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'post-thumbnails' ); 

    
    //-----------------------------------------------------
    // Disable WYSIWYG
    //-----------------------------------------------------

    add_action('admin_head', 'hide_editor'); 
    
    function hide_editor() { 
      if( get_post_type() == 'team-member' ) { ?> 
        <style> #mceu_30-body, #ed_toolbar { display:none; } </style> 
        <?php 
      } 
    }

    //-----------------------------------------------------
    // Hide editor on Team page
    //-----------------------------------------------------
    
    add_action( 'admin_init', 'hide_content_editor' );

    function hide_content_editor() {
      // Get the Post ID.
      $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
      if( !isset( $post_id ) ) return;

      // Hide the editor on the page titled 'Homepage'
      $homepgname = get_the_title($post_id);
      if($homepgname == 'The Team'){ 
        remove_post_type_support('page', 'editor');
      }

      // Hide the editor on a page with a specific page template
      // Get the name of the Page Template file.
      $template_file = get_post_meta($post_id, '_wp_page_template', true);

      if($template_file == 'my-page-template.php'){ // the filename of the page template
        remove_post_type_support('page', 'editor');
      }
    }    

    //-----------------------------------------------------
    // Page Slug Body Class
    //-----------------------------------------------------
    
    function add_slug_body_class( $classes ) {
      global $post;
      if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
      }
      
      return $classes;
    }
    
    add_filter( 'body_class', 'add_slug_body_class' );   

    //-----------------------------------------------------
     // WP Footer
     //----------------------------------------------------- 

    function relish_wp_footer() {
      $footer_markup;      
      
      /* Facebook div */
      $footer_markup .= '<div id="fb-root"></div>';

      echo $footer_markup;
    }

    add_action('wp_footer', 'relish_wp_footer');

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
     */
    //add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'primary' => __( 'Primary Menu', 'relish' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
    ) );

    /*
     * Enable support for Post Formats.
     * See http://codex.wordpress.org/Post_Formats
     */
    add_theme_support( 'post-formats', array(
      'aside', 'image', 'video', 'quote', 'link',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'relish_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    ) ) );    
  }
  endif; // relish_setup
  add_action( 'after_setup_theme', 'relish_setup' );


  function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/login.css' );
  }
  add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
