<?php 

  //-----------------------------------------------------
  // Frontpage Banner Slides
  //-----------------------------------------------------

  function setup_theme_admin_menus() {

      add_menu_page('Theme settings', 'Banner Slides', 'manage_options', 
              'tut_theme_settings', 'theme_settings_page', 'dashicons-slides', 6 ); 

      function theme_settings_page() {
      ?>
        <div class="wrap">
          <label for="num_elements">
              Number of elements on a row:
          </label> 
           
          <input type="text" name="num_elements" />        
        </div>
      <?php
      }             
  }
   
  add_action("admin_menu", "setup_theme_admin_menus");

?>