<?php
/**
 * @package relish
 */

//-----------------------------------------------------
// Get Thumbnail
//-----------------------------------------------------

if (has_post_thumbnail()) {
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full');  
}

//-----------------------------------------------------
// Get PDF Link
//-----------------------------------------------------


$pdf_link = get_field('menu_pdf');

if (!empty($pdf_link)) {
  $pdf_link = $pdf_link['url'];
} else {
  $pdf_link = get_permalink();
}

?>

<article <?php post_class(); ?>>
  <?php the_title( '<h3 class="entry-title"><a href="' . esc_url( $pdf_link ) . '" rel="bookmark">', '</a></h3>' ); ?>
  
  <?php if (has_post_thumbnail()): ?>
    <a href="<?= $pdf_link; ?>" style="background-image:url(<?= $thumb[0] ?>)" class="thumbnail"></a>
  <?php endif ?>    
</article>