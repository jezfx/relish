<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

global $post; 

//-----------------------------------------------------
// Banner Page
//-----------------------------------------------------

$banner = get_field( 'add_to_homepage_banner', $post->ID );

if ( !empty( $banner ) ) {
  $banner_sub_title = get_field( 'sub_title', $post->ID );
  $banner_image =   get_field( 'image', $post->ID );
}

//-----------------------------------------------------
// Page Intro Paragraph
//-----------------------------------------------------

$page_intro_paragraph = get_field( 'page_intro_paragraph', $post->ID );

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
  		
      <div class="inner">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <header class="entry-header">
            
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

            <?php if ( $banner_sub_title ): ?>
              <h2><?= $banner_sub_title ?></h2>              
            <?php endif ?>

              <div class="post-info">
                <div class="single-post-info-links"><a href="/news">&laquo; Back</a> | <?= get_the_date() . ' | ' . ucfirst(get_the_author()) . ' | ' . relish_get_categories(); ?></div>
                <div class="share-buttons">
                  <div class="fb-like" data-href="https://www.facebook.com/pages/Relish-Cafe-Bar/293002197382814" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>       
                  <a href="https://twitter.com/share" class="twitter-share-button" data-via="Jezfx" data-count="none">Tweet</a>         
                </div>
              </div>

          </header><!-- .entry-header --> 

          <div class="entry-content">

            <?php if ( $banner_image ): ?>
              <figure>
                <img src="<?= $banner_image['url'] ?>" alt="<?= the_title(); ?>">
              </figure>            
            <?php endif ?>          
            
            <?php if (has_post_thumbnail() && (!in_category('featured'))): ?>
              <figure>
                <?= the_post_thumbnail('full', array('class'=> 'featured')); ?>
              </figure>
            <?php endif ?>  
            
            <?php the_content(); ?>
            
            <?php
              wp_link_pages( array(
                'before' => '<div class="page-links">' . __( 'Pages:', 'relish' ),
                'after'  => '</div>',
              ) );
            ?>
          </div><!-- .entry-content -->

        </article><!-- #post-## -->				
			</div>		

	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
