<?php
/**
 * The template for displaying all posts.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

global $post; 

//-----------------------------------------------------
// Page Intro Paragraph
//-----------------------------------------------------

$page_intro_paragraph = get_field('page_intro_paragraph', $post->ID);

//-----------------------------------------------------
// Get News Items
//-----------------------------------------------------

$featured_category_id = get_cat_id('featured');

$args = array(
  'cat'     => '-' . $featured_category_id,
  'orderby' => 'menu_order',
  'order'   => 'ASC'
);

$query = new WP_Query( $args );

get_header(); ?>

    <div class="inner menus">
      <section <?php post_class('posts'); ?>> 
        
        <header class="entry-header">
        	<h1>Latest News</h1>
        </header>

        <?php if (!empty($page_intro_paragraph)): ?>
          <p class="page-intro-paragraph">
            <?= get_field('paragraph'); ?>
          </p>  
        <?php endif; ?>  

        <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

            <?php get_template_part('content'); ?>

        <?php endwhile; ?>

        <?= relish_pagination(); ?>

        <?php endif; ?>
          
      </section>       
    </div>    

<?php get_footer(); ?>
