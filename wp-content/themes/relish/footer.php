<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package relish
 */
?>
    </section> <!-- content -->
  </div> <!-- wrapper -->
  <?php wp_footer(); ?>    
</body>
</html>