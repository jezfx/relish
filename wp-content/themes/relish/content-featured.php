<?php
/**
 * @package relish
 */
?>

<article <?php post_class(); ?>>

  <?php if (has_post_thumbnail()): ?>
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full'); ?>
    <a href="<?php the_permalink(); ?>" style="background-image:url(<?= $thumb[0] ?>)" class="thumbnail"></a>
  <?php endif ?>    
  
</article>