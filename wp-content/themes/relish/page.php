<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

global $post; 

//-----------------------------------------------------
// Page Intro Paragraph
//-----------------------------------------------------

$page_intro_paragraph = get_field('page_intro_paragraph', $post->ID);

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
  		
      <div class="inner">
				<?php include(locate_template('content-page.php')); ?>				
			</div>		

	<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>