<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package relish
 */

global $post;

//-----------------------------------------------------
// Get Banners
//-----------------------------------------------------

$args_banner = array(
	'type' => 'post',
		'meta_query' => array(
			'relation' => 'AND',
				array(
					'key' => 'add_to_homepage_banner',
					'value' => true,
					'compare' => '='
				)
		)
);

$banners = new WP_Query($args_banner);


//-----------------------------------------------------
// Get Page Content
//-----------------------------------------------------

$content = get_post($post->ID);


//-----------------------------------------------------
// Get Blog Posts
//-----------------------------------------------------

$posts_args = array(
	'category_name' 	=> 'news',
	'posts_per_page' 	=> 2,
	'orderby'   			=> 'menu_order',
  'order'     			=> 'ASC'
);              

$news_posts = new WP_Query( $posts_args );


$featured_posts_args = array(
	'category_name' 	=> 'featured',
	'posts_per_page' 	=> 2
);

$featured_posts = new WP_Query( $featured_posts_args );


get_header(); ?>

	<?php if ( $banners->have_posts() ): ?>
		<section class="slider-promo">
			<ul id="slider">
				<?php while( $banners->have_posts() ) : $banners->the_post(); ?>
					
					<?php $image = get_field( 'image' ); ?>	
					
					<li class="<?= strtolower( get_field('theme') ); ?>">
						<div class="caption-container">
							
							<div class="cf">
								<a href="<?= the_permalink(); ?>">
									<h1><?= get_field( 'main_title' ) ?></h1>
								</a>
							</div>
							
							<a href="<?= get_the_permalink(); ?>">
								<h2><?= get_field( 'sub_title' ) ?></h2>
							</a>
						</div>
						
						<a href="<?= the_permalink(); ?>">
							<img src="<?= $image['url'] ?>" alt="<?= $image['alt']; ?>">
						</a>

					</li>					
				
				<?php endwhile; ?>
			</ul>
		</section>					
	<?php endif; wp_reset_query(); ?>

	<div class="promo-callout">
		<h1><?= get_the_title(); ?></h1>
		<p><?= $content->post_content; rewind_posts(); ?></p>
	</div>

	<div class="inner">

		<section <?php post_class('posts'); ?>>
			<h2 class="section-heading">Latest News</h2>
			
			<div class="frontpage-posts news">
				<?php while ( $news_posts->have_posts() ) : $news_posts->the_post(); ?>
					<?php get_template_part('content', get_post_format()); ?>
				<?php endwhile; rewind_posts(); ?>	
			</div>

			<div class="frontpage-posts featured">
				<?php while ( $featured_posts->have_posts() ) : $featured_posts->the_post(); ?>
					<?php get_template_part('content', 'featured'); ?>
				<?php endwhile; ?>
			</div>				
		</section>

    <div class="pagination recent">
      <div class="recent"><a href="/news">View More News &rsaquo;</a></div>
    </div>		
	</div>

<?php get_footer(); ?>
