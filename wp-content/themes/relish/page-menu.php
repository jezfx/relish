<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package relish
 */

global $post; 

//-----------------------------------------------------
// Page Intro Paragraph
//-----------------------------------------------------

$page_intro_paragraph = get_field('page_intro_paragraph', $post->ID);


//-----------------------------------------------------
// Get Menus
//-----------------------------------------------------

$args = array(
  'post_type' => 'restaurant-menu',
  'orderby'   => 'menu_order',
  'order'     => 'ASC'
);

$menus = new WP_Query($args);

get_header(); ?>

    <div class="inner menus">
      
      <section <?php post_class('posts'); ?>> 
        
        <header class="entry-header">
          <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->

        <?php if (!empty($page_intro_paragraph)): ?>
          <p class="page-intro-paragraph">
            <?= get_field('paragraph'); ?>
          </p>  
        <?php endif; ?>  

        <?php if ( $menus->have_posts() ) : while ( $menus->have_posts() ) : $menus->the_post(); ?>           

            <?php include(locate_template('content-menu.php')); ?>

        <?php endwhile; ?>

        <!-- post navigation -->

        <?php endif; ?>
          
      </section>
       
    </div>    

<?php get_footer(); ?>
